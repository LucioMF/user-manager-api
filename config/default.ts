export default {
  HOST: 'http://localhost',
  PORT: 8080,
  // MONGO_URI: 'mongodb://localhost/UserManager',
  MONGO_URI: 'mongodb://34.69.232.230:27017,34.69.232.230:27018,34.69.232.230:27019/UserManager?replicaSet=rs0',
  JWT_EXP_TIME: '6h',
  WEB_URL: 'http://localhost:4200',

  EMAIL_HOST: 'smtp.gmail.com',
  EMAIL_PORT: '587',
  EMAIL_TLS: 'true',
  EMAIL_USER: 'chatprojdev@gmail.com',
  EMAIL_PASSWORD: 'ChatProjectD3v',
};
